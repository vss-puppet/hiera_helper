# Class: hiera_helper::vhost
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# hiera_helper::snmp:
#   agentaddress: 
#     - udp:161
#   ro_community: notpublic
#   ro_network: 10.20.30.40/32
#   contact: root@yourdomain.org
#   location: Phoenix, AZ
class hiera_helper::snmp {

  $hiera_config = hiera_hash('hiera_helper::snmp', undef)

  validate_hash( $hiera_config )
  create_resources('snmp',$hiera_config)
}

