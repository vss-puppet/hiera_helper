# Class: hiera_helper::firewall
#
# This class collects all data from hiera for puppetlabs/apache firewall
# define type.
#
# Parameters
#
# firewall:      A hash of all firewall rules
# firewallchain: A hash of firewall chains to add
# firewall_pre:  A hash of rules to be run first (i.e. purge rules)
# firewall_post: A hash of rules to be ran last (i.e. drop all)
#
# Usage
#
# hiera_helper::firewall_pre:
#   firewall:
#     purge: 'true'
#
# hiera_helper::firewall:
#  '000 accept all icmp requests':
#    proto: 'icmp'
#    action: 'accept'
#  '100 allow http and https access':
#    port:
#      - '80'
#      - '443'
#    proto: 'tcp'
#    action: 'accept'
#
# hiera_helper::firewall_post:
#  '999 drop all':
#    proto: 'all'
#    action: 'drop'
#    before: 'undef'
class hiera_helper::firewall {

  $hiera_firewallchain = hiera_hash('hiera_helper::firewallchain', {})
  $hiera_firewall      = hiera_hash('hiera_helper::firewall', {})
  $hiera_firewall_pre  = hiera_hash('hiera_helper::firewall_pre', {})
  $hiera_firewall_post = hiera_hash('hiera_helper::firewall_post', {})

  validate_hash( $hiera_firewallchain )
  validate_hash( $hiera_firewall )
  validate_hash( $hiera_firewall_pre )
  validate_hash( $hiera_firewall_post )

  create_resources('firewall',$hiera_firewall_pre)
  create_resources('firewallchain',$hiera_firewallchain)
  create_resources('firewall',$hiera_firewall)
  create_resources('firewall',$hiera_firewall_post)
  create_resources('firewall',$hiera_config)
}

