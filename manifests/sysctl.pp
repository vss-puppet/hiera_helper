# Class: hiera_helper::sysctl
#
# This class collects all data from hiera for the class package. The
# default action is ensure is set to install.
#
# Parameters
#
# names: a hash of sysctl and their parameters
#
# Usage
#
# In hiera config:
#
# hiera_helper::sysctl:
#   fs.file-max:
#     value: 6815744
#   net.ipv4.ip_local_port_range:
#     value: 9000 65500
#
class hiera_helper::sysctl (
  ) {
    $defaults = {
      permanent => 'yes',
    }

    $hiera_config = hiera_hash('hiera_helper::sysctl', undef)

    validate_hash( $hiera_config )
    create_resources(::sysctl, $hiera_config, $defaults)
}

