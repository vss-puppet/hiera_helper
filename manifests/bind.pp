# Class: hiera_helper::vhost
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# hiera_helper::bind:
#   /etc/named.conf:
#     listen_on_addr: 
#       - any
#     forwarders:
#       - 8.8.8.8
#       - 8.8.4.4
class hiera_helper::bind {

  $hiera_config = hiera_hash('hiera_helper::bind', undef)

  validate_hash( $hiera_config )
  create_resources('bind::server::conf',$hiera_config)
}

