# Class: hiera_helper::vhost
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# hiera_helper::ssl:
#     cn: name.nam3.msu.edu
class hiera_helper::ssl {

  $hiera_config = hiera_hash('hiera_helper::ssl', undef)

  validate_hash( $hiera_config )
  create_resources('ssl::cert',$hiera_config)
}

