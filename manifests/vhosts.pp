# Class: hiera_helper::vhosts
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# hiera_helper::vhosts:
#   example.com:
#     port: 80
#     docroot: /var/www/example.com
class hiera_helper::vhosts {

  $hiera_config = hiera_hash('hiera_helper::vhosts', undef)

  validate_hash( $hiera_config )
  create_resources('apache::vhost',$hiera_config)
}

