# Class: hiera_helper::rhnreg
#
# This class collects all data from hiera for the module
# mthibaut/users. See http://forge.puppetlabs.com/mthibaut/users
# for more information.
#
# Parameters
#
# register: a hash of registration information
#
# Usage
#
# In hiera config:
#
# hiera_helper::rhnreg:
#   ${fqdn}:
#     activationkeys: 'lklkasdjjflakjsdfl'
#     server_url: https://example.com/XMLRPC'
#     ssl_ca_cert: '/path/to/TRUSTED_SSL_CERT'
#
#
class hiera_helper::rhnreg {

  $hiera_config = hiera_hash('hiera_helper::rhnreg', undef)

  validate_hash( $hiera_config )
  create_resources('rhn_register',$hiera_config)
}

