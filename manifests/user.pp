# Class: hiera_helper::vhost
#
# This class collects all data from hiera for the module
# mthibaut/users. See http://forge.puppetlabs.com/mthibaut/users
# for more information.
#
# Parameters
#
# groups:  A hash of all vhosts
#
# Usage
#
# In hiera config:
#
# hiera_helper::users::groups:
#   sysadmin:
#
# Remember, it's not the actual users you are specifying, just
# the name of the hash you wish to use.
#
# Read http://forge.puppetlabs.com/mthibaut/users
#
class hiera_helper::user {

  $hiera_config = hiera_hash('hiera_helper::users', undef)

  validate_hash( $hiera_config )
  create_resources('users',$hiera_config)
}

